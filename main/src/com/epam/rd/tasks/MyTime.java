package com.epam.rd.tasks;

public class MyTime {
    private int hour;
    private int minute;
    private int second;

    public MyTime(int hour, int minute, int second) {
        this.hour = validateHour(hour);
        this.minute = validateMinute(minute);
        this.second = validateSecond(second);
    }

    private static int validateTime(int givenDigit, int maxAllowedDigit) {
        return givenDigit > maxAllowedDigit? 0: givenDigit;
    }

    private static int validateHour(int hour) {
        return validateTime(hour, 23);
    }

    private static int validateMinute(int minute) {
        return validateTime(minute, 59);
    }

    private static int validateSecond(int second) {
        return validateTime(second, 59);
    }

    public void setTime(int hour, int minute, int second) {
        this.hour = validateHour(hour);
        this.minute = validateMinute(minute);
        this.second = validateSecond(second);
    }

    public void setHour(int hour) {
        this.hour = validateHour(hour);
    }

    public void setMinute(int minute) {
        this.minute = validateMinute(minute);
    }

    public void setSecond(int second) {
        this.second = validateSecond(second);
    }

    private static int convertToSecs(int hour, int minute) {
        return minute * 60 + hour * 3600;
    }

    private void fromSecs(int seconds) {
        int minutes = seconds / 60;
        int hours = seconds / 3600;
        this.second = seconds % 60;
        this.minute = minutes % 60;
        this.hour = hours % 24;
    }

    private int toSecs() {
        int secs = this.second;
        int minutes = this.minute * 60;
        int hours = this.hour * 3600;
        return secs + minutes + hours;
    }

    public void addSeconds(int seconds) {
        fromSecs(this.toSecs() + seconds);
    }
    public void subtractSeconds(int seconds) {
        fromSecs(this.toSecs() - seconds);
    }

    public void addMinutes(int minutes) {
        int addSeconds = convertToSecs(0, minutes);
        fromSecs(this.toSecs() + addSeconds);
    }

    public void subtractMinutes(int minutes) {
        int addSeconds = convertToSecs(0, minutes);
        fromSecs(this.toSecs() - addSeconds);
    }

    public void addHours(int hours) {
        int addSeconds = convertToSecs(hours, 0);
        fromSecs(this.toSecs() + addSeconds);
    }

    public void subtractHours(int hours) {
        int addSeconds = convertToSecs(hours, 0);
        fromSecs(this.toSecs() - addSeconds);
    }

    public void add(MyTime other) throws UnsupportedOperationException {
        if (other == null || getClass() != other.getClass()) {
            throw new UnsupportedOperationException("Cannot add objects of different classes.");
        }
        fromSecs(this.toSecs() + other.toSecs());
    }

    public void subtract(MyTime other) throws UnsupportedOperationException {
        if (other == null || getClass() != other.getClass()) {
            throw new UnsupportedOperationException("Cannot subtract objects of different classes.");
        }
        int resSecs = this.toSecs() - other.toSecs();
        while (resSecs < 0) {
            resSecs += 86400; // if time is negative we have to add a 24-hour offset to the result
        }
        fromSecs(resSecs);
    }

    public String showTime() {
        return String.format("%02d:%02d:%02d", this.hour, this.minute, this.second);
    }
}
