package com.epam.rd.tasks;

public class Test2 {

    private int firstVariable;
    private int secondVariable;

    public Test2() {
        this.firstVariable = 0;
        this.secondVariable = 0;
    }

    public Test2(int _firstVariable, int _secondVariable) {
        this.firstVariable = _firstVariable;
        this.secondVariable = _secondVariable;
    }

    public int getFirstVariable() {
        return firstVariable;
    }

    public void setFirstVariable(int firstVariable) {
        this.firstVariable = firstVariable;
    }

    public int getSecondVariable() {
        return secondVariable;
    }

    public void setSecondVariable(int secondVariable) {
        this.secondVariable = secondVariable;
    }
}
