package com.epam.rd.tasks;

public class Test1 {

    private int firstVariable;
    private int secondVariable;

    public Test1() {
        this.firstVariable = 0;
        this.secondVariable = 0;
    }

    public Test1(int _firstVariable, int _secondVariable) {
        this.firstVariable = _firstVariable;
        this.secondVariable = _secondVariable;
    }

    public int getFirstVariable() {
        return firstVariable;
    }

    public void setFirstVariable(int firstVariable) {
        this.firstVariable = firstVariable;
    }

    public int getSecondVariable() {
        return secondVariable;
    }

    public void setSecondVariable(int secondVariable) {
        this.secondVariable = secondVariable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Test1 test1 = (Test1) o;

        if (firstVariable != test1.firstVariable) {
            return false;
        }
        return secondVariable == test1.secondVariable;
    }

    @Override
    public int hashCode() {
        int result = firstVariable;
        result = 31 * result + secondVariable;
        return result;
    }

    @Override
    public String toString() {
        return String.format("Test1{firstVariable=%s, secondVariable=%s}", firstVariable, secondVariable);
    }

    public int calculateSumOfAttributes() {
        return firstVariable + secondVariable;
    }

    public int findLargestVariable() {
        if(firstVariable > secondVariable) {
            return firstVariable;
        }
        if(firstVariable == secondVariable) {
            System.out.println("Class variables are equal.");
            return 0;
        }
        return secondVariable;
    }
}
