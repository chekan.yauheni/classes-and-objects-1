package com.epam.rd.tasks;

public class Triangle {

    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double sideA, double sideB, double sideC) throws InstantiationException {
        boolean validTriangle = validateTriangle(sideA, sideB, sideC);
        if(!validTriangle) {
            throw new InstantiationException("Triangle inequality theorem violation! Check the sides values.");
        }
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    private static boolean validateTriangle(double a, double b, double c) {
        return (a + b) > c && (a + c) > b && (b + c) > a;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) throws InstantiationException {
        boolean validTriangle = validateTriangle(sideA, this.sideB, this.sideC);
        if(!validTriangle) {
            throw new InstantiationException("Triangle inequality theorem violation! Check the sides values.");
        }
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) throws InstantiationException {
        boolean validTriangle = validateTriangle(this.sideA, sideB, this.sideC);
        if(!validTriangle) {
            throw new InstantiationException("Triangle inequality theorem violation! Check the sides values.");
        }
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) throws InstantiationException {
        boolean validTriangle = validateTriangle(this.sideA, this.sideB, sideC);
        if(!validTriangle) {
            throw new InstantiationException("Triangle inequality theorem violation! Check the sides values.");
        }
        this.sideC = sideC;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Triangle triangle = (Triangle) o;

        if (Double.compare(triangle.sideA, sideA) != 0) {
            return false;
        }
        if (Double.compare(triangle.sideB, sideB) != 0) {
            return false;
        }
        return Double.compare(triangle.sideC, sideC) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(sideA);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sideB);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sideC);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return String.format("Triangle{sideA=%s, sideB=%s, sideC=%s}\n", sideA, sideB, sideC);
    }

    public double calculateArea() {
        double semiPerimeter = calculatePerimeter() / 2;
        return Math.sqrt(semiPerimeter * (semiPerimeter - sideA) * (semiPerimeter - sideB) * (semiPerimeter - sideC));
    }

    public double calculatePerimeter() {
        return sideA + sideB + sideC;
    }
}

