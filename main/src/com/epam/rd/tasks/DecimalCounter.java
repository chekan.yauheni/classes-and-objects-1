package com.epam.rd.tasks;

public class DecimalCounter {

    private int startCounter;
    private final int stopCounter;
    private int currState = startCounter;

    public DecimalCounter() {
        this.stopCounter = Integer.MAX_VALUE;
    }

    public DecimalCounter(int _startCounter) {
        this.startCounter = _startCounter;
        this.stopCounter = Integer.MAX_VALUE;
    }

    public DecimalCounter(int _startCounter, int _stopCounter) {
        this.startCounter = _startCounter;
        this.stopCounter = (_stopCounter > _startCounter)? _stopCounter: _startCounter + 1;
    }

    public String showRange() {
        return String.format("DecimalCounter range: (%s: %s)\n", startCounter, stopCounter);
    }

    public int getState() {
        return this.currState;
    }

    public int getStartCounter() {
        return startCounter;
    }

    public int getStopCounter() {
        return stopCounter;
    }

    public void increment() {
        if (this.currState >= this.stopCounter) {
            return;
        }
        this.currState++;
    }

    public void decrement() {
        if (this.currState <= this.startCounter) {
            return;
        }
        this.currState--;
    }

    @Override
    public String toString() {
        return String.format("DecimalCounter{start: %s, stop: %s}\n", startCounter, stopCounter);
    }
}
