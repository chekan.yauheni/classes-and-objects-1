package com.epam.rd;

import com.epam.rd.tasks.DecimalCounter;
import com.epam.rd.tasks.MyTime;
import com.epam.rd.tasks.Test1;
import com.epam.rd.tasks.Triangle;

public class Main {
    public static void main(String[] args) throws InstantiationException {
        demonstrateTest1();

        demonstrateTriangle();

        demonstrateCounter();

        demonstrateTime();
    }

    public static void demonstrateTest1() {
        Test1 testClass1 = new Test1();

        testClass1.setFirstVariable(9);
        testClass1.setSecondVariable(93);

        System.out.println(testClass1);

        int largestVar = testClass1.findLargestVariable();
        System.out.printf("The largest variable of the class = %s\n", largestVar);

        int sumOfVars = testClass1.calculateSumOfAttributes();
        System.out.printf("The sum of class variables = %s\n", sumOfVars);
    }

    public static void demonstrateTriangle() throws InstantiationException {
        Triangle newTriangle = new Triangle(20, 32, 45);

        System.out.printf("\n%s", newTriangle);

        double trianglePerimeter = newTriangle.calculatePerimeter();
        System.out.printf("Perimeter = %s\n", trianglePerimeter);

        double triangleArea = newTriangle.calculateArea();
        System.out.printf("Area = %s\n", triangleArea);
    }

    public static void demonstrateCounter() {
        DecimalCounter counter1 = new DecimalCounter();

        System.out.printf("\n%s", counter1.showRange());
        System.out.printf("Counter1 current state: %s\n", counter1.getState());

        counter1.increment();
        counter1.increment();
        System.out.printf("Counter1 current state: %s\n", counter1.getState());

        DecimalCounter counter2 = new DecimalCounter(0);
        System.out.printf("\n%s", counter2.showRange());
        System.out.printf("Counter2 current state: %s\n", counter2.getState());

        counter2.increment();
        counter2.increment();
        System.out.printf("Counter2 current state: %s\n", counter2.getState());
        counter2.increment();
        System.out.printf("Counter2 current state: %s\n", counter2.getState());

        for (int n = 0; n < 18; n++) {
            counter2.decrement();
        }
        System.out.printf("Counter2 current state: %s\n", counter2.getState());
    }

    public static void demonstrateTime() {
        MyTime time1 = new MyTime(12, 5, 70);

        System.out.printf("\nTime1 = %s\n", time1.showTime());

        time1.setHour(22);
        time1.setMinute(31);
        time1.setSecond(59);

        System.out.printf("Time1 set to -> %s\n", time1.showTime());

        time1.setTime(2, 14, 8);
        System.out.printf("Time1 set to -> %s\n", time1.showTime());

        time1.addMinutes(46);
        System.out.printf("Added 46 minutes to Time1 -> %s\n", time1.showTime());

        MyTime time2 = new MyTime(2, 59, 51);
        time1.add(time2);

        System.out.printf("Added %s to Time1 -> %s\n", time2.showTime(), time1.showTime());

        MyTime time3 = new MyTime(23, 0, 0);

        time1.subtract(time3);
        System.out.printf("Subtracted %s from Time1 -> %s\n", time3.showTime(), time1.showTime());
    }
}