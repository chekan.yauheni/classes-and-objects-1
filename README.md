# Task 1. 
## Methods of output, finding the sum and the largest value
### Solve the task:

- [x] Create a `Test1` class with two variables. 
- [x] Add a method for displaying and methods for changing these variables. 
- [x] Add a method that finds the sum of the values of these variables, and a method that finds 
      the largest value of these two variables.

# Task 2.
## Set- and get- methods
### Solve the task:

- [x] Create a Test2 class with two variables. 
- [x] Add a constructor with input parameters. 
- [x] Add a constructor that initializes the default class members. 
- [x] Add set- and get- methods for the instance fields of the class.

# Task 3.
## Triangle
### Solve the task:

- [x] Describe the class representing the triangle. (A triangle is described by three sides.)
- [x] Provide methods for creating objects, calculating the area, perimeter.

# Task 4.
## Decimal counter
### Solve the task:

- [x] Describe a class implementing a decimal counter that can increase or decrease its value by one in a given range.
- [x] Provide initialization of the counter with default values and arbitrary values. 
- [x] The counter has methods for increasing and decreasing the state, and a method for getting its current state. 
- [x] Write code that demonstrates all the features of the class.

# Task 5.
## Time representation
### Solve the task:

- [x] Create a class description to represent the time. 
- [x] Provide for the possibility of setting the time and changing its individual fields (hour, minute, second)
with checking the validity of the entered values. 
- [x] In case of invalid field values, the field is set to 0. 
- [x] Create methods for changing the time by a specified number of hours, minutes and seconds.
